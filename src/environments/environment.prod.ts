export const environment = {
  production: true,
  mapboxKey: 'pk.eyJ1IjoibmFodWVsZSIsImEiOiJja2RteGl3dDYxZDg4MnFsY2JrbmczbTVyIn0.cmv_eh2S5gMk-jXPNbuDNg', // eje capas
  firebase: {
    apiKey: "AIzaSyCu0Uq4FqtPkgiblDUuGsOJ2_eq1LJtPmo",
    authDomain: "mapaproyectos-59125.firebaseapp.com",
    databaseURL: "https://mapaproyectos-59125.firebaseio.com",
    projectId: "mapaproyectos-59125",
    storageBucket: "mapaproyectos-59125.appspot.com",
    messagingSenderId: "1028006325784",
    appId: "1:1028006325784:web:b017a9a52ad41e28085d6d",
    measurementId: "G-MJ2FJJTB3D"
  }
};
